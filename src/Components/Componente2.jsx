import '../Components/estilos.css'
import avatar from '../assets/img/avatar.svg';


function Componente2() {
    return (

        <div className="Componente2">
            <div className='row'>
                <section className='page-section fondo col-sm'>
                    <section className='page-section text-white mb-0' id="about">
                        <div className='container'>
                            <div className='card shadow mb-4'>
                                <div className='card-header py-3'>
                                    <h6 className='m-0 font-weight-bold text-primary comp text-black'>
                                        Competencias
                                        generales</h6>
                                </div>
                                <div className='card-body'>
                                    <h4 className='small font-weight-bold text-black'>Liderazgo: <span
                                        className='float-right'>80%</span></h4>
                                    <div className='progress mb-4'>
                                        <div className='progress-bar bg-danger liderazgo' role="progressbar"
                                            aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <h4 className='small font-weight-bold text-black'>Pensamiento Crítico: <span
                                        className='float-right'>78%</span></h4>
                                    <div className='progress mb-4'>
                                        <div className='progress-bar bg-warning pensa' role="progressbar"
                                            aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <h4 className='small font-weight-bold text-black'>Manejo de Conflicto: <span
                                        className='float-right'>86%</span></h4>
                                    <div className='progress mb-4'>
                                        <div className='progress-bar conflicto' role="progressbar"
                                            aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <h4 className='small font-weight-bold text-black'>Adaptación: <span
                                        className='float-right'>90%</span></h4>
                                    <div className='progress mb-4'>
                                        <div className='progress-bar bg-info adapta' role="progressbar"
                                            aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <h4 className='small font-weight-bold text-black'>Creatividad: <span
                                        className='float-right'>92%</span></h4>
                                    <div className='progress'>
                                        <div className='progress-bar bg-success creatividad' role="progressbar"
                                            aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <br />
                </section>


                <div className='col-sm masthead fondo text-white text-center'>
                    <div className='container d-flex align-items-center flex-column'>
                        <img className='masthead-avatar mb-2' src={avatar} alt="..." width="100" height="100" />
                        <h1>Porcayo Espinoza Alicia</h1>
                        <div className='divider-custom divider-light'>
                            <div className='divider-custom-line'></div>
                            <div className='divider-custom-icon'><i class="fas fa-star"></i></div>
                            <div className='divider-custom-line'></div>
                        </div>
                        <p className='masthead-subheading font-weight-light mb-0'>ING. En Sistemas Computacionales</p>
                    </div>
                </div>






                <section className='page-section2 fondo col-sm'>
                    <div className='container '>
                        <div className='row'>
                            
                            <section className='page-section text-white mb-0' id="about">
                                <div className='container col'>
                                    
                                    <div className='card shadow tama'>
                                    <h6 className='text-black text-center'>Software</h6>
                                        <div className='row sfw'>
                                            
                            
                                            <div className='col-lg-9 mb-2 java' >
                                                <div className='card bg-primary text-black shadow '>
                                                    <div className='card-body'>
                                                        JAVA
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='col-lg-9 mb-3 '>
                                                <div className='card bg-success text-white shadow'>
                                                    <div className='card-body'>
                                                        PYTHON
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='col-lg-9 mb-3'>
                                                <div className='card bg-info text-white shadow'>
                                                    <div className='card-body'>
                                                        HTML
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='col-lg-9 mb-3'>
                                                <div className='card bg-warning text-white shadow'>
                                                    <div className='card-body'>
                                                        SQL
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='col-lg-9 mb-3'>
                                                <div className='card bg-danger text-white shadow'>
                                                    <div className='card-body'>
                                                        MONGO DB
                                                    </div>
                                                </div>
                                            </div>

                                            <div className='col-lg-9 mb-3'>
                                                <div className='card bg-light text-black shadow'>
                                                    <div className='card-body'>
                                                        POSTMAN
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='col-lg-9 mb-3' >
                                                <div className='card bg-dark text-white shadow'>
                                                    <div className='card-body'>
                                                        GIT
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </section>
                        </div>
                    </div>

                    <br />
                </section>





            </div>

        </div>

    )
}

export default Componente2

