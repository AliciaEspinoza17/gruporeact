import foto from '../assets/img/foto.jpg'
import guitarra from '../assets/img/guitarra.jpg'
import genshin from '../assets/img/genshin.jpg'
import libro from '../assets/img/libro.jpg'
import fotoaly from '../assets/img/fotoaly.png'
export default{
    "img1":foto,
    "img2":guitarra,
    "img3":genshin,
    "img4":libro,
    "img11":fotoaly
}