import jardin from "../img/jardin.jpg"
import tania from '../img/tania.jpg'
import leer from '../img/leer.png'
import musi from '../img/musi.png'
import paraca from '../img/paraca.jpg'

export default {
    "img1": tania,
    "img2": jardin,
    "img3": musi,
    "img4": leer,
    "img5": paraca,
}