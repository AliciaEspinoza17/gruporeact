import denny from "../img/denyy.jpeg"
import peli from '../img/peli.jpg'
import adeled2 from '../img/adeled2.jpg'
import escr from '../img/escr.jpg'
import ori from '../img/ori.jpg'

export default {
    "img1": denny,
    "img2": peli,
    "img3": adeled2,
    "img4": escr,
    "img5": ori,
}