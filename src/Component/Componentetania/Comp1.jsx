import React from 'react'
import '../Componentetania/Comp1.css'
import imagen from '../../assets/img/imagen'

export default function Comp1() {
    return (
    <>
    <div className="tcontainer2">
    <div className="tone2">
        <img className="timgp" src={imagen.img1} alt="" />
    </div>
    <div className="ttwo2">
        <h1>Mora Tlaseca Tania Yamilet</h1>
        <h3>19680203</h3>
    </div>
    <br/>
  </div>
  <div className="tcontainer">
    <div className="tone">
      <div className="card" >
        <img className="card-img-top" src={imagen.img2} alt="Card image cap" width="200" height="250"/>
        <div className="card-body">
          <h5 className="card-title">Jardineria</h5>
          <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModaljardin">
            Más
          </button>
        </div>
      </div>
    </div>
    <div className="ttwo">
      <div className="card">
        <img className="card-img-top"  src={imagen.img3} alt="Card image cap" width="200" height="250" />
        <div className="card-body">
          <h5 className="card-title">Escuchar musica</h5>
          <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalmusica">
            Más
          </button>
        </div>
      </div>
    </div>
    <div className="ttree">
      <div className="card" >
        <img className="card-img-top" src={imagen.img4} alt="Card image cap" width="200" height="250" />
        <div className="card-body">
          <h5 className="card-title">Leer</h5>
          <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalleer">
            Más
          </button>
        </div>
      </div>
    </div>
    <div className="tfour">
      <div className="card" >
        <img className="card-img-top" src={imagen.img5} alt="Card image cap" width="200" height="250" />
        <div className="card-body">
          <h5 className="card-title">Paracaidismo </h5>
          <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalparacaidismo">
            Más
          </button>
        </div>
      </div>
    </div>
  </div>
  <div className="modal fade" id="exampleModaljardin" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div className="modal-dialog">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title" id="exampleModalLabel">HOBBY: JARDINERIA</h5>
          <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div className="modal-body">
          <h5>Es lo que mas disfruto realizar, no es trabajo pesado, solo cuidar plantas y lo que conlleva</h5>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  
  <div className="modal fade" id="exampleModalmusica" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div className="modal-dialog">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title" id="exampleModalLabel">HOBBY: ESCUCHAR MUSICA</h5>
          <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div className="modal-body">
          <h4>Disfruto mucho de la musica que no tengo favoritismo, me gusta escuchar de todo tipo</h4>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  
  <div className="modal fade" id="exampleModalleer" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div className="modal-dialog">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title" id="exampleModalLabel">HOBBY: LEER</h5>
          <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div className="modal-body">
          <h4>No he leido muchos libros, pero es un pasatiempo que me agrada bastante</h4>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div className="modal fade" id="exampleModalparacaidismo" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div className="modal-dialog">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title" id="exampleModalLabel">HOBBY: PARACAIDISMO</h5>
          <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div className="modal-body">
          <h4>No es una actividad que realice frecuentemente, pero es algo que me agrada mucho</h4>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
    </>
    )
}
