import '../ComponenteEli/ComponenteEli.css'
import imagene from '../../assets/img/imagene.js'

function ComponenteEli(){
    return(
        <>
            <div className="econtainer2">
                <div className="tone2">
                <img className="timgp" src={imagene.img1} alt="" />
                </div>
                <div className="ttwo2">
                    <h1 className='t'>Eli Juarez Rodríguez</h1>
                    <h3 className='t'>19680175</h3>
                </div>
                <br/>
            </div>
    <div className="econtainer">
    <div className="tone">
      <div className="card" >
        <img className="card-img-top" src={imagene.img2} alt="Card image cap" width="200" height="250"/>
        <div className="card-body">
          <h5 className="card-title">VideoJuegos</h5>
          <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#games">
            Más
          </button>
        </div>
      </div>
    </div>
    <div className="ttwo">
      <div className="card">
        <img className="card-img-top"  src={imagene.img3} alt="Card image cap" width="200" height="250" />
        <div className="card-body">
          <h5 className="card-title">Peliculas y Series</h5>
          <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#pelis">
            Más
          </button>
        </div>
      </div>
    </div>
    <div className="ttree">
      <div className="card" >
        <img className="card-img-top" src={imagene.img4} alt="Card image cap" width="200" height="250" />
        <div className="card-body">
          <h5 className="card-title">Anime</h5>
          <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#ani">
            Más
          </button>
        </div>
      </div>
    </div>
    <div className="tfour">
      <div className="card" >
        <img className="card-img-top" src={imagene.img5} alt="Card image cap" width="200" height="250" />
        <div className="card-body">
          <h5 className="card-title">Musica</h5>
          <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#musi">
            Más
          </button>
        </div>
      </div>
    </div>
    </div>

    <div className="modal fade" id="games" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div className="modal-dialog">
        <div className="modal-content">
        <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">VideoJuegos</h5>
            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div className="modal-body">
            <h5>Mis videojuegos favoritos son los Shooters y los de mundo abierto, entre ellos estan: </h5>
            <ol>
                                        <li>Call of duty</li>
                                        <li>Halo</li>
                                        <li>Battlefield</li>
                                        <li>Spiderman PS4</li>
                                        <li>God of War</li>
                                        <li>Gears of War</li>
                                    </ol>
        </div>
        <div className="modal-footer">
            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
    </div>

    <div className="modal fade" id="pelis" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div className="modal-dialog">
        <div className="modal-content">
        <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">Peliculas</h5>
            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div className="modal-body">
            <h5>Mis peliculas favoritas son las de SuperHeroes o Accion y algunas de terror,
                                    en las series casi es lo mismo, me gustan las comedias y las de accion y suspenso. </h5>
                                    <ol>
                                        <li>Batman</li>
                                        <li>La teoria del big bang</li>
                                        <li>Barbie</li>
                                        <li>Wolverine</li>
                                        <li>X-men</li>
                                        <li>Avengers</li>
                                    </ol>
        </div>
        <div className="modal-footer">
            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
    </div>

    <div className="modal fade" id="ani" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div className="modal-dialog">
        <div className="modal-content">
        <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">Anime</h5>
            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div className="modal-body">
            <h5>Los animes que más me agrada ver suelen ser lo de comedia, algo de comedia-romantica,
                                    accion, aventura y un poco los de suspenso. </h5>
                                    <ol>
                                        <li>Kono Subarashii Sekai ni Shukufuku wo!</li>
                                        <li>Kobayashi-san Chi no Maid Dragon</li>
                                        <li>Attack on Titan</li>
                                        <li>Watashi ga Motenai no wa Dō Kangaetemo Omaera ga Warui!</li>
                                        <li>Uzaki-chan wa Asobitai!</li>
                                        <li>My Hero Academia</li>
                                    </ol>
        </div>
        <div className="modal-footer">
            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
    </div>

    <div className="modal fade" id="musi" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div className="modal-dialog">
        <div className="modal-content">
        <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">Musica</h5>
            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div className="modal-body">
            <h5>La musica que llego a escuchar por lo general es rock, un poco de electronica, pop
                                    y me gusta tocar la guitarra. </h5>
                                    <ol>
                                        <li>All Good Things</li>
                                        <li>Katy Perry</li>
                                        <li>Lady Gaga</li>
                                        <li>Adele</li>
                                        <li>Bryce Fox</li>
                                        <li>Imagine Dragons</li>
                                    </ol>
        </div>
        <div className="modal-footer">
            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
    </div>
        </>
    )
}
export default ComponenteEli