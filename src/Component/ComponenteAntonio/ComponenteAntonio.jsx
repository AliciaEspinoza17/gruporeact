import { useState } from 'react'
import './ComponenteAntonio.css'
import images from '../../assets/imagenes.js'

function Tony() {
    const [count, setCount] = useState(0)

    return (
        <>
        <div className='b'>
            <div className='container2'>
                <div className='one2'>
                    <img className='imgC' src={images.img1} alt="" />
                </div>
                <div className='two2'>
                    <h2>FLORES SOLIS ANTONIO EMMANUEL</h2>
                    <h3>19680145</h3>
                </div>
            </div>
            <div className='container'>
                <div className='one'>
                    <div className='card'>
                        <img className='card-img-top' src={images.img3} alt="Card image cap" />
                        <div className='card-body'>
                            <h5 className='card-title'>VIDEOJUEGOS</h5>
                            <button type="button" className='btn btn-primary' data-bs-toggle="modal" data-bs-target="#exampleModal">
                                Informacion
                            </button>
                        </div>
                    </div>
                </div>
                <div className='two'>
                    <div className='card'>
                        <img className='card-img-top' src={images.img2} alt="Card image cap" />
                        <div className='card-body'>
                            <h5 className='card-title'>GUITARRA</h5>
                            <button type="button" className='btn btn-primary' data-bs-toggle="modal" data-bs-target="#exampleModal2">
                                Informacion
                            </button>
                        </div>
                    </div>
                </div>
                <div className='tree'>
                    <div className='card'>
                        <img className='card-img-top' src={images.img4} alt="Card image cap" />
                        <div className='card-body'>
                            <h5 className='card-title'>LECTURA</h5>
                            <button type="button" className='btn btn-primary' data-bs-toggle="modal" data-bs-target="#exampleModal3">
                                Informacion
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div className='modal fade' id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">HOBBY: Jugar Videojuegos</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <h4>Uno de los principales hobbys que tengo es jugar videojuegos, los juegos a los que he invertido mas horas
          son:<br />
          Genshin Impact<br />
          Saga de Final Fantasy</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">HOBBY: Tocar Guitarra</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <h4>Una habilidad la cual aun estoy en proceso de practica es tocar la guitarra, es una habilidad la cual sigo
          aprendiendo y mejorando al dia de hoy</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModal3" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">HOBBY: Leer</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <h4>Un hobby que he adquirido reciente mente es la lectura, aunque no he leido una gran cantidad de libros es un
          hobby el cual cada dia disfruto mas</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</div>
        </>
    )
}

export default Tony
