import React from 'react'
import './Josh.css'
import imagen from '../../assets/imgJ/imge'

function Josh() {
    return (
        <div className='contenedor'>Josh
            <div className='Foto'>

            </div>
            <h1>Herminio Vazquez Gustavo Josuhar</h1>
            <div className='cardPadre'>
                <div className="cards">
                    <div className='mov'>
                        <h1 className='t'>Musica</h1>

                        <h4 className='text'>
                            Se puede decir
                            que me gusta todo tipo de musica,
                            pero la que escucho mas es el rap.

                        </h4>

                    </div>
                    <img src={imagen.m} alt="" className='mm' />
                </div>
                <div className="cards">
                    <div className='mov'>
                        <h1 className='t'>Deporte</h1>
                        <h4 className='text'>
                            El deporte que mas me gusta es el baloncesto,puesto que se me hace un deporte
                            muy estilizado.
                        </h4>
                    </div>
                    <img src={imagen.D} alt="" className='mm' />
                </div>
                <div className="cards">
                    <div className='mov'>
                        <h1 className='t'>Paisaje</h1>
                        <h4 className='text'>Me gusta caminar por lugares, tranquilos y
                            rodeados por la naturaleza.</h4>
                    </div>
                    <img src={imagen.p} alt="" className='mm' />
                </div>
                <div className="cards">
                    <div className='mov'>
                        <h1 className='t'>Programar</h1>
                        <h4 className='text'>   Estar en la computadora escrbiendo lineas de codigo,
                            se me hace un trabajo muy estimulante.</h4>
                    </div>
                    <img src={imagen.P} alt="" className='mm' />
                </div>

            </div>


        </div>
    )
}

export default Josh